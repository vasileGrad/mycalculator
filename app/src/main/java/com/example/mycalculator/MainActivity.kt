package com.example.mycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.lang.ArithmeticException

class MainActivity : AppCompatActivity() {

    var lastNumeric : Boolean = false
    var lastDot : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onDigit(view: View) {
        val tvInput = findViewById<TextView>(R.id.tvInput)
        tvInput.append((view as Button).text)
        lastNumeric = true
    }

    fun onClear(view: View) {
        val tvInput = findViewById<TextView>(R.id.tvInput)
        tvInput.text = ""
        lastNumeric = false
        lastDot = false
    }

    fun onDecimalPoint(view: View) {
        if(lastNumeric && !lastDot) {
            val tvInput = findViewById<TextView>(R.id.tvInput)
            tvInput.append(".")
            lastNumeric = false
            lastDot = true
        }
    }

    fun onEqual(view: View) {
        if (lastNumeric) {
            val tvInputText = findViewById<TextView>(R.id.tvInput)
            var tvValue = tvInputText.text.toString()
            var prefix = ""

            try {
                if (tvValue.startsWith("-")) {
                    prefix = "-"
                    tvValue = tvValue.substring(1)
                }
                if(tvValue.contains("-")) {
                    operation("-",prefix, tvValue, tvInputText)
                } else if(tvValue.contains("+")) {
                    operation("+",prefix, tvValue, tvInputText)
                } else if(tvValue.contains("*")) {
                    operation("*",prefix, tvValue, tvInputText)
                } else if(tvValue.contains("/")) {
                    operation("/",prefix, tvValue, tvInputText)
                }
            } catch (e: ArithmeticException) {
                e.printStackTrace()
            }
        }
    }

    private fun operation(operation: String, prefix: String, tvValue: String,tvInputText: TextView) {
        val splitValue = tvValue.split(operation)
        var one = splitValue[0]
        var two = splitValue[1]

        if(!prefix.isEmpty()) {
            one = prefix + one
        }

        when(operation) {
            "-" -> tvInputText.text = removeZeroAfterDot((one.toDouble() - two.toDouble()).toString())
            "+" -> tvInputText.text = removeZeroAfterDot((one.toDouble() + two.toDouble()).toString())
            "*" -> tvInputText.text = removeZeroAfterDot((one.toDouble() * two.toDouble()).toString())
            "/" -> tvInputText.text = removeZeroAfterDot((one.toDouble() / two.toDouble()).toString())
        }
    }

    private fun removeZeroAfterDot(result: String) : String {
        var value = result
        if (result.contains(".0"))
            value = result.substring(0, result.length-2)
        return value
    }

    fun onOperator(view: View) {
        val tvInput = findViewById<TextView>(R.id.tvInput)
        if(lastNumeric && !isOperatorAdded(tvInput.text.toString())) {
            tvInput.append((view as Button).text)
            lastNumeric = false
            lastDot = false
        }
    }

    private fun isOperatorAdded(value: String) : Boolean {
        return if (value.startsWith("-")) {
            false
        } else {
            value.contains("/") || value.contains("*")
                    || value.contains("+") || value.contains("-")
        }
    }
}